import 'package:dio/dio.dart';
import 'package:sw_auth/endpoint/endpoint_auth_token.dart';
import 'package:sw_auth/endpoint/endpoint_polled_auth.dart';
import 'package:sw_auth/service/service_token.dart';
import 'package:sw_auth/usecase_polled_auth.dart';
import 'package:sw_core/interface/service/i_service_token.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/interface/usecase/i_usecase_polled_auth.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/sw_init_core.dart';
import 'package:sw_core/tool/http_client.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/log.dart';

import 'endpoint/endpoint_auth.dart';
import 'endpoint/endpoint_auth_refresh_token.dart';
import 'usecase_auth.dart';

Future initAuth({RequestAuthToken? requestToken, String? token}) async {
  initCore();
  _initControllerPolledAuth();
  _initController();
  await _initToken(requestToken, token);
}

Future _initToken(RequestAuthToken? requestToken, String? token) async {
  if (requestToken != null) {
    wtf("Initializing using partner provided token endpoint!");
    await _initServiceWithRequest(requestToken);
    return;
  }
  if (token != null) {
    wtf("Initializing using partner provided token string!");
    await _initServiceWithToken(token);
    return;
  }
  error("No token or token endpoint was provided, token service couldn't be initialized!");
}

Future _initServiceWithRequest(RequestAuthToken requestToken) async {
  put<IServiceToken>(
    () => ServiceToken(
      endToken: EndpointAuthToken(defaultHTTPClient(), baseUrl: requestToken.endpoint),
      endRefreshToken: EndpointAuthRefreshToken(defaultHTTPClient()),
      requestToken: requestToken,
      baseURL: requestToken.endpoint,
    ),
  );
  await get<IServiceToken>().getAuthToken();
}

Future _initServiceWithToken(String token) async =>
    put<IServiceToken>(() => ServiceToken(token: token));

Dio _getDioAdapter() {
  Dio _adapter = get<Dio>();
  // _adapter.options = BaseOptions(validateStatus: _validateStatus);
  return _adapter;
}

void _initControllerPolledAuth() =>
    put<IUseCasePolledAuth>(() => UseCasePolledAuth(EndpointPolledAuth(_getDioAdapter())));

// bool _validateStatus(int? status) => status != null ? status <= 304 : false;

void _initController() =>
    put<IUseCaseAuth>(() => UseCaseAuth(EndpointAuth(get<Dio>()), get<IUseCasePolledAuth>()));
