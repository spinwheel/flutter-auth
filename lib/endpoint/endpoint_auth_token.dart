import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/response/response_auth_token.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_auth_token.g.dart';

@RestApi(baseUrl: 'http://localhost:8081/')
abstract class EndpointAuthToken {
  factory EndpointAuthToken(
    Dio dio, {
    String baseUrl,
  }) = _EndpointAuthToken;

  @POST('')
  Future<ResponseAuthToken> requestToken(
    @Body() RequestAuthToken authentication,
  );
}
