// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'endpoint_auth_refresh_token.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _EndpointAuthRefreshToken implements EndpointAuthRefreshToken {
  _EndpointAuthRefreshToken(this._dio, {this.baseUrl}) {
    baseUrl ??= get<SWEnvironment>().url;
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ResponseAuthToken> refreshToken(authentication) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(authentication.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<ResponseAuthToken>(
        Options(method: 'POST', headers: _headers, extra: _extra)
            .compose(_dio.options, '/v1/dim/token/refresh',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResponseAuthToken.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
