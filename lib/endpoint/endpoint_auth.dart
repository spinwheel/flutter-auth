import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/request/request_auth.dart';
import 'package:sw_core/request/request_auth_add_steps.dart';
import 'package:sw_core/response/response_auth.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_auth.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointAuth {
  factory EndpointAuth(Dio dio, {String baseUrl}) = _EndpointAuth;

  @POST(login)
  Future<ResponseAuth> requestAuth(
    @Body() RequestAuth authentication,
  );

  @POST(login)
  Future<ResponseAuth> requestAuthAddSteps(
    @Body() RequestAuthAddSteps authentication,
  );

  @GET(user)
  Future<ResponseAuth> getUser(
    @Query(extUserId) String extUserId,
  );
}
