import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/request/request_auth_refresh_token.dart';
import 'package:sw_core/response/response_auth_token.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_auth_refresh_token.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointAuthRefreshToken {
  factory EndpointAuthRefreshToken(
    Dio dio, {
    String baseUrl,
  }) = _EndpointAuthRefreshToken;

  @POST('/v1/dim/token/refresh')
  Future<ResponseAuthToken> refreshToken(
    @Body() RequestAuthRefreshToken authentication,
  );
}
