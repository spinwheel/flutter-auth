// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'endpoint_polled_auth.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _EndpointPolledAuth implements EndpointPolledAuth {
  _EndpointPolledAuth(this._dio, {this.baseUrl}) {
    baseUrl ??= get<SWEnvironment>().url;
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ResponseAuth> requestPolledAuth(taskId) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch(_setStreamType<ResponseAuth>(
        Options(method: 'GET', headers: _headers, extra: _extra)
            .compose(_dio.options, '/v1/pollingTasks/$taskId',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    print(_result.data);
    final value = ResponseAuth.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
