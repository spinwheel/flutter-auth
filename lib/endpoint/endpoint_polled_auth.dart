import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/response/response_auth.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_polled_auth.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointPolledAuth {
  factory EndpointPolledAuth(Dio dio, {String baseUrl}) = _EndpointPolledAuth;

  @GET("$polled_login{taskId}")
  Future<ResponseAuth> requestPolledAuth(
    @Path("taskId") String taskId,
  );
}
