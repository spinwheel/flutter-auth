import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_user/unintegrated_user.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/interface/usecase/i_usecase_polled_auth.dart';
import 'package:sw_core/request/request_auth.dart';
import 'package:sw_core/request/request_auth_add_steps.dart';
import 'package:sw_core/response/response_auth.dart';
import 'package:sw_core/tool/response_handler.dart';
import 'package:sw_core/usecase/usecase_base.dart';

import 'endpoint/endpoint_auth.dart';

class UseCaseAuth extends UseCaseBase with ResponseHandler implements IUseCaseAuth {
  UseCaseAuth(this._endpointAuth, this._controllerPolledAuth);

  final EndpointAuth _endpointAuth;
  final IUseCasePolledAuth _controllerPolledAuth;

  @override
  Rx<AuthStep> authStep = AuthStep.STEP_UNAUTHORIZED.obs;

  @override
  Rx<User> user = User().obs;

  @override
  Rx<SecurityStep> security = SecurityStep().obs;

  @override
  Rx<Object> error = Object().obs;

  @override
  RxBool isPolledTask = false.obs;

  @override
  Rx<UnintegratedUser> unintegratedUser = UnintegratedUser().obs;

  @override
  requestAuth(RequestAuth requestAuth) =>
      _authenticate(() => _endpointAuth.requestAuth(requestAuth));

  @override
  requestAuthAddSteps(RequestAuthAddSteps requestAuth) =>
      _authenticate(() => _endpointAuth.requestAuthAddSteps(requestAuth));

  @override
  getUser(String extUserID) => _authenticate(() => _endpointAuth.getUser(extUserID));

  _authenticate(Future<ResponseAuth> Function() fetch) => handle<ResponseAuth>(
        fetch: fetch,
        onValue: _onValue,
        onError: _onAuthError,
      );

  _onValue(ResponseAuth response) async {
    if (response.user != null) {
      isPolledTask.value = false;
      user.value = response.user!;
      authStep.value = AuthStep.STEP_AUTHORIZED;
      return;
    }

    if (response.security != null) {
      isPolledTask.value = false;
      security.value = response.security!;
      authStep.value = AuthStep.STEP_SECURITY;
      return;
    }

    if (response.taskId != null) {
      isPolledTask.value = true;
      if (response.taskId!.taskId != null) {
        await _controllerPolledAuth.requestPolledAuth(response.taskId!.taskId!);
        user.value = _controllerPolledAuth.user.value;
      }
    }
  }

  _onAuthError(Object e) {
    error.value = e;
    authStep.value = AuthStep.AUTH_ERROR;
    isPolledTask.value = false;
  }
}
