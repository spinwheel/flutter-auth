import 'dart:core';

import 'package:dio/dio.dart';
import 'package:sw_core/usecase/usecase_base.dart';
import 'package:sw_core/entity_auth/error.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/interface/usecase/i_usecase_polled_auth.dart';
import 'package:sw_core/response/response_auth.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/log.dart';
import 'package:sw_core/tool/response_handler.dart';

import 'endpoint/endpoint_polled_auth.dart';

class UseCasePolledAuth extends UseCaseBase with ResponseHandler implements IUseCasePolledAuth {
  UseCasePolledAuth(this._endpointPolledAuth);

  final EndpointPolledAuth _endpointPolledAuth;

  @override
  final authStep = AuthStep.STEP_UNAUTHORIZED.obs;

  @override
  final user = User().obs;

  @override
  final error = Error().obs;

  @override
  final isPolling = false.obs;

  final _response = ResponseAuth().obs;

  @override
  Future requestPolledAuth(String taskID) =>
      _authenticate(() => _endpointPolledAuth.requestPolledAuth(taskID));

  _authenticate(Future<ResponseAuth> Function() fetch) => handle<ResponseAuth>(
        fetch: fetch,
        onValue: _onValue,
        onError: _onError,
      );

  _retry() async {
    info("Processing task...");
    await Future.doWhile(_processTask);
  }

  Future<bool> _processTask() async => await Future.delayed(Duration(seconds: 3), _task);

  Future<bool> _task() async {
    info("3s has passed, retrying...");
    return _endpointPolledAuth
        .requestPolledAuth(_response.value.taskId!.id!)
        .then(_checkSnapshotValue);
  }

  bool _checkSnapshotValue(ResponseAuth value) {
    bool hasError = value.error != null;
    bool hasUser = user.value.extUserId != null;
    if (value.user != null) {
      user.value = value.user!;
    }
    hasError ? error.value = value.error! : error.value = Error();
    return hasError || hasUser ? false : true;
  }

  _onValue(ResponseAuth response) async {
    _response.value = response;

    if (_response.value.taskId != null) {
      isPolling.value = true;
      await _retry();
    }

    if (user.value.extUserId != null) {
      get<IUseCaseAuth>().authStep.value = AuthStep.STEP_AUTHORIZED;
      authStep.value = AuthStep.STEP_AUTHORIZED;
    } else {
      get<IUseCaseAuth>().authStep.value = AuthStep.AUTH_ERROR;
      authStep.value = AuthStep.AUTH_ERROR;
    }
    isPolling.value = false;
  }

  _onError(Object e) {
    if (e is DioError) {
      error.value = Error(code: e.response?.statusCode, desc: e.response?.statusMessage);
    }
    authStep.value = AuthStep.AUTH_ERROR;
    isPolling.value = false;
  }
}
