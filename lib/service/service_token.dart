import 'dart:async';

import 'package:get/get.dart';
import 'package:sw_auth/endpoint/endpoint_auth_refresh_token.dart';
import 'package:sw_auth/endpoint/endpoint_auth_token.dart';
import 'package:sw_core/entity_token/token.dart';
import 'package:sw_core/interface/service/i_service_token.dart';
import 'package:sw_core/request/request_auth_refresh_token.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/response/response_auth_token.dart';
import 'package:sw_core/tool/data_path.dart';

class ServiceToken extends GetxService implements IServiceToken {
  ServiceToken({
    EndpointAuthToken? endToken,
    EndpointAuthRefreshToken? endRefreshToken,
    RequestAuthToken? requestToken,
    String token = '',
    String baseURL = base_SW_URL,
  })  : _endToken = endToken,
        _endRefreshToken = endRefreshToken,
        requestToken = requestToken,
        token = token.obs,
        baseURL = baseURL {
    if (token != '') {
      this._decodedToken = DecodedToken.fromToken(token);
    }
    _setupAutoRefresh();
  }

  @override
  String baseURL;

  final EndpointAuthToken? _endToken;
  final EndpointAuthRefreshToken? _endRefreshToken;

  @override
  RequestAuthToken? requestToken;

  @override
  bool get handleToken => requestToken != null && _endToken != null && _endRefreshToken != null;

  @override
  RxString token = ''.obs;

  @override
  String clientId = '';

  @override
  String refreshToken = '';

  Timer? _timer;

  DecodedToken _decodedToken = DecodedToken();

  @override
  void onClose() {
    super.onClose();
    _timer?.cancel();
  }

  @override
  void getAuthToken() async {
    if (handleToken) {
      final String token = _updateTokens(
        await _endToken!.requestToken(requestToken!),
      );
      _decodedToken = DecodedToken.fromToken(token);
      clientId = _decodedToken.clientId;
    }
  }

  @override
  void refreshAuthToken([Timer? timer]) async {
    if (handleToken) {
      if (token.isEmpty) {
        return getAuthToken();
      }
      _updateTokens(
        await _endRefreshToken!.refreshToken(RequestAuthRefreshToken(refreshToken: refreshToken)),
      );
    }
  }

  String _updateTokens(ResponseAuthToken responseToken) {
    token.value = responseToken.tokenData.token;
    refreshToken = responseToken.tokenData.refreshToken;
    return token.value;
  }

  void _setupAutoRefresh() {
    if (handleToken) {
      _timer = Timer.periodic(
        Duration(seconds: requestToken!.tokenExpiryInSec - 60),
        refreshAuthToken,
      );
    }
  }
}
