@echo off

echo ________________________________      DELETE PUBSPEC.LOCK AUTH
call del "pubspec.lock"
echo ________________________________      FLUTTER CLEAN AUTH
call flutter clean
echo ________________________________      FLUTTER PUB UPGRADE AUTH
call flutter pub upgrade
echo ________________________________      FLUTTER PUB GET AUTH
call flutter pub get